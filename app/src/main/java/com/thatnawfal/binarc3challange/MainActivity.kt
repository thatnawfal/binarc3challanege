package com.thatnawfal.binarc3challange

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    val aWord = arrayListOf(
        Word('A', arrayListOf("Ayam", "Akar", "Abadi")),
        Word('B', arrayListOf("Bakar", "Buku", "Biawak")),
        Word('C', arrayListOf("Cukur", "Cicak", "Cantik")),
        Word('D', arrayListOf("Dodol", "Dadu", "Dukun")),
        Word('E', arrayListOf("Ekor", "Empang", "Endok")),
        Word('G', arrayListOf("Goreng", "Gagak", "Gulung")),
        Word('H', arrayListOf("Horden", "Hamba", "Harus")),
        Word('J', arrayListOf("Jera", "Jarum", "Jantung")),
        Word('L', arrayListOf("London", "Labil", "Loker")),
        Word('N', arrayListOf("Nakal", "Nirwana", "Nabi"))
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}