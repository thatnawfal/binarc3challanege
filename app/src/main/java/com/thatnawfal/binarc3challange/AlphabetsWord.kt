package com.thatnawfal.binarc3challange

data class AlphabetsWord(
    var alphabet: Char,
    var wordOfAlphabet: List<String>
)
