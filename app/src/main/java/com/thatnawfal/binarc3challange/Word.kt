package com.thatnawfal.binarc3challange

data class Word(
    val alphabets: Char,
    val listOfWord: ArrayList<String>
)
